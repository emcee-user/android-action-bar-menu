package com.example.actionbarmenu;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getActionBar();
		actionBar.setTitle("ActionBar");
		
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#009688")));
		setContentView(R.layout.activity_main);
		
	}
// Add menu items to action bar
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
// onclick functionality for item
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		switch (item.getItemId()) {
		case R.id.home:
			Toast.makeText(getApplicationContext(), "Clicked on "+item.getTitle() +" Item", Toast.LENGTH_SHORT).show();
			break;
		case R.id.cart:
			Toast.makeText(getApplicationContext(), "Clicked on "+item.getTitle() +" Item", Toast.LENGTH_SHORT).show();
			break;
		case R.id.aboutus:
			Toast.makeText(getApplicationContext(), "Clicked on "+item.getTitle() +" Item", Toast.LENGTH_SHORT).show();
			break;
		case R.id.contactus:
			Toast.makeText(getApplicationContext(), "Clicked on "+item.getTitle() +" Item", Toast.LENGTH_SHORT).show();
			break;
		case R.id.phone:
			Toast.makeText(getApplicationContext(), "Clicked on "+item.getTitle() +" Item", Toast.LENGTH_SHORT).show();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
